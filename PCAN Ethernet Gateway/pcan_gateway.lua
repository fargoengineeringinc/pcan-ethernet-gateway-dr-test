-----------------------------------------------------------------------------------------------------
-- @file	pcan_gateway.lua
-- @author	U.Wilhelm
-- @see		www.peak-system.com
-- @brief	Lua Dissector for PEAK-System Ethernet and Wireless Gateways
-- @version	1.0.1
--
--
-- PEAK Ethernet & Wireless Gateway Protcol  Dissector Version 1.0.1
-- simple CAN over Ethernet Protocol dissector for products IPEH-004010 IPEH-004011, IPEH-004020(A)
-- (c) 2016 by PEAK-System Technik GmbH 
-- This Software is open source - feel free to share and change it for your need!
--
-- Support UDP or TCP with multiple Ports
-- Default Port Range is 52000 to 52010
-- Default Protocol is UDP
--
-- HOW TO RUN THIS SCRIPT:
-- Wireshark and Tshark support multiple ways of loading Lua scripts: through a dofile() call in init.lua,
-- through the file being in either the global or personal plugins directories, or via the command line.
-- See the Wireshark USer's Guide chapter on Lua (https://wiki.wireshark.org/Lua).
-- Here the lines that you need to at at the end of the init.lua script (change the directory for your need):
--
--                 -- Add PEAK Network CAN Protocol at startup
--                 PEAK_PROTO_SCRIPT_PATH="D:\\WiresharkPortable\\"
--                 dofile(PEAK_PROTO_SCRIPT_PATH.."pcan-gateway.lua")
--
-- Once the script is loaded, it creates a new protocol named "peak-can" (or "PEAK-CAN" in some places).
-- If you have a capture file with packets in it which have data from a gateway, simply select one in the Packet List pane,
-- right-click on it, and select "Decode As ...", and then in the dialog box that shows up scroll down the list of protocols to one
-- called "PEAK-CAN", select that and click the "ok" or "apply" button. 
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
-- "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
-- TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
-- CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
-- EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
-- PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
-- WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
-- OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-----------------------------------------------------------------------------------------------------

-- declare our protocol
PEAK_Ethernet_proto = Proto("PEAK-CAN","CAN Ethernet Gateway Protocol")

-- setup parameters
protocol_udp = 1
protocol_tcp = 2

-- available setting options for protocol 
local pref_protocol_enum = {
    { 1,  "UDP", protocol_udp },
    { 2,  "TCP", protocol_tcp  },
}

-- default values for settings - change for your need!
local default_settings =
{
    expert_enabled	= false, 			-- default the Expert mode is off
    ports         	= "52000 - 52010",	-- default port(s) number for CAN Ethernet Packages - change for your need - accept also range settings..see LUA description
    protocol		= protocol_udp, 	-- default we use UDP 
}

-- Settings for UI settings
PEAK_Ethernet_proto.prefs.infotext = Pref.statictext(
	"A PEAK-System Ethernet and WLAN Gateway Dissector for WireShark",
	"www.peak-system.com"
	)
	
PEAK_Ethernet_proto.prefs.can_ports = Pref.range(
	"Ports",
	default_settings.ports,
	"PCAN-Ethernet Listener Port",
	65535
	)
	
PEAK_Ethernet_proto.prefs.expert_mode = Pref.bool(
	"Show all available Info (Expert mode)",
	default_settings.expert_enabled,
	"Show also non user Data of protocol"
	)
	
PEAK_Ethernet_proto.prefs.transport_protokol = Pref.enum(
	"Select the used Protocol",
	default_settings.protocol,
	"set the UDP or TCP as protocol - TCP need a listener to get work",
	pref_protocol_enum
	)

-- PlugIn Info
local PEAK_Ethernet_plugin_info = {
            version = "1.0.1",
			description = "PEAK-System Ethernet and Wireless Gateway Protocol Filter",
            author = "U.Wilhelm",
			company = "PEAK-System Technik Germany",
            company_url = "http://www.peak-system.com"
			}
set_plugin_info(PEAK_Ethernet_plugin_info)
			
			
-- create a function to dissect it
function PEAK_Ethernet_proto.dissector(buffer,pinfo,tree)

	-- CONST
	local const_can_2ab = 0x80				-- 0x80 defines a CAN2.0A/2.0B Frame - FD will come soon...
	local const_can_packet_lenght = 0x24 	-- 36 Byte is the lengt of a CAN Frame in the Buffer

	-- VAR local
	local dlc								-- Stores the DLC of CAN Frame
	local tmp_dlc							-- Counter for generating buffer for DataBytes
	local db_info_txt						-- Buffer for all DataBytes
	local index = 0							-- Counter for CANMsg inFrame
	local can_data_frame_len = buffer:len() -- Length of Data Buffer in Frame
	local can_frame_counter					-- Counter of stored CAN Frames in Data Buffer
	local can_type							-- PEAK Internal Format 0x80 is Std CAN2.0A/2,0B - CAN-FD will come soon	
	local can_id							-- The CAN ID
	local can_msg_type						-- The Msg Type (11 or 29 Bit)
	local can_rtr_flag						-- Flag if RTR
		
	can_type = buffer(2,2):uint()			-- get the CAN Type
	if can_type ~=  const_can_2ab then  	-- check if CAN2.0A/2.0B
	  local subtree_main = tree:add(PEAK_Ethernet_proto,buffer(),"No CAN2.0A/2.0B Protocol Data!")
	  subtree_main:add_expert_info(PI_PROTOCOL,PE_ERROR,"Not a PEAK-System compatible CAN2.0A/2.0B Frame")
	  return false -- get out of here...
	end 
	-- Calculate the CAN Messages in complete package
	can_frame_counter = can_data_frame_len/const_can_packet_lenght
	
	-- New Tree Frame Information
	local subtree_main = tree:add(PEAK_Ethernet_proto,buffer(),"CAN 2.0a/b Protocol Data - PEAK-System Format")
	subtree_main:add(PEAK_Ethernet_proto,buffer(0,can_data_frame_len),"Complete CAN Data size in Frame" , "Length: " .. can_data_frame_len )
    subtree_main:add(PEAK_Ethernet_proto,buffer(0,2),"CAN Frames: " .. can_frame_counter )
	-- Set the Colums in the Receive List
	if default_settings.protocol ==  protocol_udp then 
		pinfo.cols.protocol = "PEAK-CAN over UDP"
	else
		pinfo.cols.protocol = "PEAK-CAN over TCP"		
	end
		
	pinfo.cols.info = "CAN Msg(s) in Frame: " .. can_frame_counter
	pinfo.cols.dst_port = default_settings.ports
	
	-- New Tree only for the real CAN Messages
	local subtree_can = subtree_main:add(PEAK_Ethernet_proto,buffer(12,can_frame_counter-12),"CAN Data Storage with " .. can_frame_counter .." entrys")	
	-- now decode the real CAN Frames
	while index < can_frame_counter do  -- loop as long as CAN Data Bloks available
		-- unused blocks will in normal mode not show - only in EXPERT moded
		if PEAK_Ethernet_proto.prefs.expert_mode == true then 
			subtree_can:add(buffer(4,8),"Tag: " .. buffer(4,8):string())
			subtree_can:add(buffer(20,1),"CAN-Channel: " .. buffer(20,1):uint())	
			subtree_can:add(buffer(22,2),"internal Flags: " .. buffer(22,2):uint())					
			subtree_can:add(buffer(2+(const_can_packet_lenght*index),2),"CAN MessageType: " .. string.format("0x%02x",buffer(2+(const_can_packet_lenght*index),2):uint()))		
		end	
		subtree_can:add(buffer(12+(const_can_packet_lenght*index),4),"TimeStamp-Low in µS: " .. buffer(12+(const_can_packet_lenght*index),4):uint())
		subtree_can:add(buffer(16+(const_can_packet_lenght*index),4),"TimeStamp-High in µS: " .. buffer(16+(const_can_packet_lenght*index),4):uint())	
		--[[
			Alernative Version to get the ID 
			get the complete 4 Bytes and use a simple AND MASK
			can_id = buffer(24+(const_can_packet_lenght*index),4):uint()
			can_id = bit32.band(can_id,0x1fffffff) -- remove the BITS 29 to 31
		]]--
		-- But i prefer this Version
		-- copy the 4 bytes int a own field
		local id_flag_buffer = buffer(24+(const_can_packet_lenght*index),4)
		-- Get the Bits 3 to 32 - keep in mind how the Data is stored !!! - correspond to Biz 0..28 in ID field
		can_id = id_flag_buffer:bitfield(3,29)
		-- Get the Bit 0 - correspond to Bit 32 in ID field
		can_msg_type = id_flag_buffer:bitfield(0,1)
		-- Get the Bit 1 - correspond to Bit 31 in ID field
		can_rtr_flag = id_flag_buffer:bitfield(1,1)
		-- show Msg.ID
		if can_msg_type == 0 then -- 11Bit ID
			subtree_can:add(buffer(24+(const_can_packet_lenght*index),4),"CAN-ID: " .. string.format("0x%03x",can_id))
		else  --29 Bit CAN ID
			subtree_can:add(buffer(24+(const_can_packet_lenght*index),4),"CAN-ID: " .. string.format("0x%08x",can_id))
		end
		-- Set Value for Msgtyoe
		if can_msg_type == 0 then
			subtree_can:add(buffer(24+(const_can_packet_lenght*index),1),"Std. 11Bit Msg.")
		else
			subtree_can:add(buffer(24+(const_can_packet_lenght*index),1),"Ext. 29Bit Msg.")
		end
		-- show if RTR Frame
		if can_rtr_flag == 1 then
			subtree_can:add(buffer(24+(const_can_packet_lenght*index),1),"RTR Frame")
		end
		-- show the CAN DLC
		dlc = buffer(21+(const_can_packet_lenght*index),1):uint()
		subtree_can:add(buffer(21+(const_can_packet_lenght*index),1),"DLC: " .. dlc)			
		tmp_dlc = 0 -- counter to zero
		db_info_txt = ""
		while tmp_dlc < dlc do  -- loop until all Data Bytes are add to buffer 
			db_info_txt = db_info_txt .. "DB" .. tmp_dlc .. ":" ..  string.format("0x%02x",buffer(28+tmp_dlc+(const_can_packet_lenght*index),1):uint()) .. " "
			tmp_dlc = tmp_dlc + 1
		end
		-- show Databytes
		subtree_can:add(buffer(28+(const_can_packet_lenght*index),8),db_info_txt)	
		-- print Seperator
		subtree_can:add("-----------------------------------------------------------------------")	
	index = index + 1
	end
end

-- Load the default port into the DissectorTable
if PEAK_Ethernet_proto.prefs.transport_protokol == protocol_udp then
	-- load the udp.port table
	udp_table = DissectorTable.get("udp.port")
	
	--register our protocol to handle udp port 
	-- udp_table:add(PEAK_Ethernet_proto.prefs.can_port,PEAK_Ethernet_proto)
	udp_table:add(PEAK_Ethernet_proto.prefs.can_ports,PEAK_Ethernet_proto)
else
	-- register the TCP port 
	tcp_table = DissectorTable.get("tcp.port")
	-- register our protocol to handle tcp port 
	--tcp_table:add(PEAK_Ethernet_proto.prefs.can_port, PEAK_Ethernet_proto)
	tcp_table:add(PEAK_Ethernet_proto.prefs.can_ports, PEAK_Ethernet_proto)
end

----------------------------------------------
-- a function for handling prefs being changed
function PEAK_Ethernet_proto.prefs_changed()

	-- remove the current Tabel entry (default_settings)
	if default_settings.protocol == protocol_udp then 
		-- UDP
		DissectorTable.get("udp.port"):remove(default_settings.ports, PEAK_Ethernet_proto)
	else
		-- TCP
		DissectorTable.get("tcp.port"):remove(default_settings.ports, PEAK_Ethernet_proto)			
	end
	

	-- Now set the new protocol with the new port 
	if PEAK_Ethernet_proto.prefs.transport_protokol == protocol_udp then			
		--UDP
		-- load the udp.port table
		udp_table = DissectorTable.get("udp.port")
		udp_table:add(PEAK_Ethernet_proto.prefs.can_ports,PEAK_Ethernet_proto)
	else
		-- TCP
		-- load the tcp.port table
		tcp_table = DissectorTable.get("tcp.port")
		tcp_table:add(PEAK_Ethernet_proto.prefs.can_ports, PEAK_Ethernet_proto)
	end
				
	-- set our new default port
	default_settings.ports = PEAK_Ethernet_proto.prefs.can_ports
	-- set our new default protocol
	default_settings.protocol = PEAK_Ethernet_proto.prefs.transport_protokol
				
end
	
