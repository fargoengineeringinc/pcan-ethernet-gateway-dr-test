﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PCAN_Ethernet_Gateway
{
    class Program
    {
        public static bool messageReceived = false;

        public struct TCANSendMsgUDP
        {

            public byte[] Length; //2 0x24 ie 36 bytes
            public byte[] MSGTYPE; //2 Fix value 0x80. This value represents a CAN data frame. 
            public byte[] Tag; //8 NOT USED
            public byte[] Time; //8 Not used for transmission
            public byte[] Channel; //1 NOT USED
            public byte[] DLC; //1 The Data Length Count (DLC) gives the length of the CAN data in bytes.
            public byte[] Flags; //2 NOT USED
            public byte[] CANID; //4
            public byte[] CANData; //8

            public TCANSendMsgUDP(int CANID)
            {
                this.Length=new byte[]{ 0,36};
                this.MSGTYPE = new byte[] { 0, 0x80 };
                this.Tag = new byte[] { 0,0,0,0,0,0,0,0 };
                this.Time = new byte[] { 0,0,0,0,0,0,0,0 };
                this.Channel = new byte[] { 0 };
                this.DLC = new byte[] { 0 };
                this.Flags = new byte[] { 0, 0 };
                this.CANID = new byte[] { 0, 0, 0, 0 };
                this.CANData = new byte[] { 0,0,0,0,0,0,0,0 };

            }

            //Bit 0 - 28 ID
            //Bit 29 Set to 0
            //Bit 30 RTR
            //Bit 31 Set to 1 for extended frame
        }


        public struct TCANMsgUDP
        {
            /// <summary>
            /// 11/29-bit message identifier
            /// </summary>
            public int ID;
            /// <summary>
            /// Type of the message
            /// </summary>
            public byte MSGTYPE;
            /// <summary>
            /// Data Length Code of the message (0..8)
            /// </summary>
            public byte LEN;
            /// <summary>
            /// Data of the message (DATA[0]..DATA[7])
            /// </summary>
            public byte[] DATA;
        }

        public struct TCANTimestampUDP
        {
            /// <summary>
            /// Base-value: milliseconds: 0.. 2^32-1
            /// </summary>
            public uint millis;
            /// <summary>
            /// Roll-arounds of millis
            /// </summary>
            public ushort millis_overflow;
            /// <summary>
            /// Microseconds: 0..999
            /// </summary>
            public ushort micros;
        }

        public static TCANMsgUDP CANFrame = new TCANMsgUDP();
        public static TCANTimestampUDP CANTime = new TCANTimestampUDP();
        public static TCANSendMsgUDP CANSendFrame = new TCANSendMsgUDP(3);

        public byte[] Length; //2 
        public byte[] MSGTYPE; //2 Fix value 0x80. This value represents a CAN data frame. 
        public byte[] Tag; //8 NOT USED
        public byte[] Time; //8 Not used for transmission
        public byte[] Channel; //1 NOT USED
        public byte[] DLC; //1 The Data Length Count (DLC) gives the length of the CAN data in bytes.
        public byte[] Flags; //2 NOT USED
        public byte[] CANID; //4
        public byte[] CANData; //8
        public static IPEndPoint remoteSender;

        public static byte[] CanSendBytes;

        public static bool runforever = true;
        public static UInt64 CANFrameCounter = 0;

        static void Main(string[] args)
        {
  
            try
            {
                int localPort = 50001; // Default value : 51001
                UdpState state;
                UdpClient client;
                //IPAddress tempAddress = IPAddress.Parse("192.168.1.10");

                 remoteSender = new IPEndPoint(IPAddress.Parse("192.168.1.10"), 50011);
                //IPEndPoint remoteSender = new IPEndPoint(IPAddress.Any, 50001);
                //remoteSender.Address = tempAddress;

                CANFrame.DATA = new byte[8];

                CanSendBytes = new byte[36] { 0x00, 0x24, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x22, 0x00, 0x08, 0x00, 0x00, 0x11, 0x11, 0x11, 0x11, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08 };
                //Array.Copy(CANSendFrame.Length, CanSendBytes, CANSendFrame.Length.Length);
                //Array.Copy(CANSendFrame.MSGTYPE, CanSendBytes, CANSendFrame.MSGTYPE.Length);
                //Array.Copy(CANSendFrame.Tag, CanSendBytes, CANSendFrame.Tag.Length);
                //Array.Copy(CANSendFrame.Time, CanSendBytes, CANSendFrame.Time.Length);
                //Array.Copy(CANSendFrame.Channel, CanSendBytes, CANSendFrame.Channel.Length);
                //Array.Copy(CANSendFrame.DLC, CanSendBytes, CANSendFrame.DLC.Length);
                //Array.Copy(CANSendFrame.Flags, CanSendBytes, CANSendFrame.Flags.Length);
                //Array.Copy(CANSendFrame.CANID, CanSendBytes, CANSendFrame.CANID.Length);
                //Array.Copy(CANSendFrame.CANData, CanSendBytes, CANSendFrame.CANData.Length);

                // Display some information
                Console.WriteLine("PEAK-System, PCAN-Ethernet Gateway UDP CAN Receive Sample.");
                Console.WriteLine("Local PORT for CAN: " + localPort);
                Console.WriteLine("Remote Host IP: " + remoteSender.Address.ToString());
                Console.WriteLine("Use '-?' to display help.");
                Console.WriteLine("Press CTRL-C to quit.\n");


                // Create UDP client CAN1
                client = new UdpClient(localPort);
                state = new UdpState(client, remoteSender);

                // Wait for receiving CAN ID 0x100 to terminate application
                do
                {
                    // Start async receiving
                    client.BeginReceive(new AsyncCallback(DataReceived), state);
                   
                 
                    while (!messageReceived)
                    {
                        ;  //do whatever you think is useful to do here...
                        Thread.Sleep(1);

                        do
                        {
                            // Start async receiving
                            client.BeginReceive(new AsyncCallback(DataReceived), state);
                            while (!messageReceived)
                            {
                                Thread.Sleep(1);
                                DataSend(client,  66808, new byte[] { 2, 0, 0, 0, 0, 0, 0, 1  });

                            }
                            messageReceived = false;
                        } while (runforever);

                    }
                    messageReceived = false;
                } while (runforever);

                Console.WriteLine("Received CAN ID 0x100 - Stop now!");
                Console.Write("CAN-Msg COUNTER: " + CANFrameCounter);
                client.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }

        private byte[] Combine(params byte[][] arrays)
        {
            byte[] rv = new byte[arrays.Sum(a => a.Length)];
            int offset = 0;
            foreach (byte[] array in arrays)
            {
                System.Buffer.BlockCopy(array, 0, rv, offset, array.Length);
                offset += array.Length;
            }
            return rv;
        }
        public static void DataSend(UdpClient c, int id, byte[] data)
        {
            //this.Length = new byte[] { 0, 36 };
            //this.MSGTYPE = new byte[] { 0, 0x80 };
            //this.Tag = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            //this.Time = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            //this.Channel = new byte[] { 0 };
            //this.DLC = new byte[] { 0 };
            //this.Flags = new byte[] { 0, 0 };
            //this.CANID = new byte[] { 0, 2, 0, 0 };
            //this.CANData = new byte[] { 1, 0, 0, 0, 0, 0, 0, 0 };

            id = 18;
            byte[] IDBytes = BitConverter.GetBytes(id);
            Array.Reverse(IDBytes);

            IDBytes[0] |= 1 << 7;// << (byte)x;
            Buffer.BlockCopy(IDBytes, 0, CanSendBytes, 24, IDBytes.Length);
            Buffer.BlockCopy(data, 0, CanSendBytes, 28, data.Length);
            c.Send(CanSendBytes, CanSendBytes.Length, remoteSender);
 

            //if (packet != null)
            //{
            //    CANFrame.ID = (packet[24] & 0x1f) << 24 | (packet[25] & 0xff) << 16 | (packet[26] & 0xff) << 8 | (packet[27] & 0xff);
            //    CANFrame.MSGTYPE = ((byte)(packet[24] >> 6));
            //    CANFrame.LEN = packet[21];
            //    if (CANFrame.LEN > 0)
            //        for (int datalen = 0; datalen < CANFrame.LEN; datalen++)
            //            CANFrame.DATA[datalen] = packet[datalen + 28];
            //    long timel = ((packet[12] & 0xff) << 24 | (packet[13] & 0xff) << 16 | (packet[14] & 0xff) << 8 | (packet[15] & 0xff));
            //    long timeh = ((packet[16] & 0xff) << 24 | (packet[17] & 0xff) << 16 | (packet[18] & 0xff) << 8 | (packet[19] & 0xff));
            //    long time = ((timeh & 0xffffffff) << 32);
            //    time |= (timel & 0xffffffffL);
            //    long millies = time / 1000;
            //    CANTime.micros = ((ushort)(time - millies * 1000));
            //    CANTime.millis = ((uint)(millies & 0xffffffff));
            //    CANTime.millis_overflow = ((ushort)(millies >> 32));
            //    return;
            //}


            //c.Send(Program.getBytes(f),36);
            ////Program.getBytes(Program.CANSendFrame);
        }
         private static void DataReceived(IAsyncResult ar)
        {

            UdpClient c = (UdpClient)((UdpState)ar.AsyncState).c;
            IPEndPoint wantedIpEndPoint = (IPEndPoint)((UdpState)(ar.AsyncState)).e;
            IPEndPoint receivedIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            Byte[] receiveBytes = c.EndReceive(ar, ref receivedIpEndPoint);

            // Check sender
            bool isRightHost = (wantedIpEndPoint.Address.Equals(receivedIpEndPoint.Address)) || wantedIpEndPoint.Address.Equals(IPAddress.Any);
            if (isRightHost) // && isRightPort)
            {
                // Convert data to ASCII and print in console
                // string receivedText = ASCIIEncoding.ASCII.GetString(receiveBytes);
                // Console.Write(receivedText);

                // Dcode RAW Data Bytes to CAN Msg Structure incl. TimeStamp from Source
                DecodeCANPacket(receiveBytes);
                // Print Time to Console
                Console.WriteLine(CANTime.millis + ":" + CANTime.micros);
                // Print CAN Data to Console
                Console.Write("ID: " + CANFrame.ID + " MsgType: " + CANFrame.MSGTYPE + " DLC: " + CANFrame.LEN + " Data: ");
                for (int count = 0; count < CANFrame.LEN; count++)
                    Console.Write(CANFrame.DATA[count] + " ");
                Console.Write("\n\n");
                // internal CAN Frame Counter
                CANFrameCounter++;
                // Stop when id 0x100 was received
                if (CANFrame.ID == 256)
                    runforever = false;// 0x100 -> STOP
            }
            else
            {
                Console.Write("IP is wrong...\n");
                Console.Write("IP: " + receivedIpEndPoint.Address + "\n");
            }
            messageReceived = true;
        }

        /* Print HELP
         */
        private static void PrintHelpText()
        {
            Console.WriteLine("CANUDPReceiver print incoming CAN data to screen.");
            Console.WriteLine("Command switches:");
            Console.WriteLine("-? : Displays this text.");
            Console.WriteLine("-lp : Set local receiving PORT for CAN. \"-lp 4001\" Default: 51001");
            Console.WriteLine("-rh : Set remote host IP. \"-rh 192.168.1.10\" Default: 0 (Any IP)");
            Console.WriteLine("\n Example of usage:\nCANoverEthernet.exe -lp 67002 -rh 192.168.1.203 ");
        }

        /* Function to decode the raw DATA into CAN and TimeStam Struct
         */
        private static void DecodeCANPacket(Byte[] packet)
        {
            if (packet != null)
            {
                CANFrame.ID = (packet[24] & 0x1f) << 24 | (packet[25] & 0xff) << 16 | (packet[26] & 0xff) << 8 | (packet[27] & 0xff);
                CANFrame.MSGTYPE = ((byte)(packet[24] >> 6));
                CANFrame.LEN = packet[21];
                if (CANFrame.LEN > 0)
                    for (int datalen = 0; datalen < CANFrame.LEN; datalen++)
                        CANFrame.DATA[datalen] = packet[datalen + 28];
                long timel = ((packet[12] & 0xff) << 24 | (packet[13] & 0xff) << 16 | (packet[14] & 0xff) << 8 | (packet[15] & 0xff));
                long timeh = ((packet[16] & 0xff) << 24 | (packet[17] & 0xff) << 16 | (packet[18] & 0xff) << 8 | (packet[19] & 0xff));
                long time = ((timeh & 0xffffffff) << 32);
                time |= (timel & 0xffffffffL);
                long millies = time / 1000;
                CANTime.micros = ((ushort)(time - millies * 1000));
                CANTime.millis = ((uint)(millies & 0xffffffff));
                CANTime.millis_overflow = ((ushort)(millies >> 32));
                return;
            }
        }

        /* Read ARGS
        */
        private static string GetValue(string[] args, ref int i)
        {
            string value = String.Empty;
            if (args.Length >= i + 2)
            {
                i++;
                value = args[i];
            }
            return value;
        }
    }

    internal class UdpState
    {
        internal UdpState(UdpClient c, IPEndPoint e)
        {
            this.c = c;
            this.e = e;
        }

        internal UdpClient c;
        internal IPEndPoint e;
    }
}
